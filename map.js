function init() {
    document.removeEventListener('DOMContentLoaded', init);

// STYLING

    // var getText = function(feature) {
    //     var text = feature.get('name');
    //     return text;
    // };
    //
    // var createTextStyle = function(feature) {
    //   return new ol.style.Text({
    //     textAlign: 'center',
    //     textBaseline: 'middle',
    //     font: '12px Verdana',
    //     text: getText(feature),
    //     fill: new ol.style.Fill({color: 'black'}),
    //     stroke: new ol.style.Stroke({color: 'white', width: 0.5})
    //   });
    // };

    // var styleRegion = new ol.style.Style({
    //     // fill: new ol.style.Fill({
    //     //     color: '#5a00b4',
    //     //     opacity: 0.1
    //     // }),
    //     stroke: new ol.style.Stroke({
    //         color: '#993367',
    //         width: 2
    //     })
    // });
    //     styleWaterbody = new ol.style.Style({
    //     fill: new ol.style.Fill({
    //         color: '#0a60b9',
    //         opacity: 0.8
    //     }),
    //     stroke: new ol.style.Stroke({
    //         color: '#0e4a88',
    //         width: 2
    //     })
    // });
        // styleCity = new ol.style.Style({
        //         image: new ol.style.RegularShape({
        //             stroke: new ol.style.Stroke({
        //               color: 'white',
        //               width: 1.5
        //             }),
        //             fill: new ol.style.Fill({
        //               color: 'black'
        //             }),
        //             radius: 5,
        //             points: 4,
        //             angle: Math.PI / 4,
        //        }),
        //       //  text: new ol.style.Text({
        //       //     color: '#FFFFFF',
        //       //     fontFamily: 'Calibri,sans-serif',
        //       //     fontSize: 12,
        //       //     text: feature.get("name").toString(),
        //       //     labelYOffset: -12
        //       // })
        //   });

    // function styleRegion(feature) {
    //     return styleRegion;
    // }

    var layerCity = 'vieneo:city',
        layerRegion = 'vieneo:region',
        layerWaterbody = 'vieneo:waterbody',
        layerTerrain = 'vieneo:terrain';

    var epsg_projection = '3857';

    // baselayers = new ol.layer.Group({
    //   title: 'Basemap-Layers',
    //   layers: [
    //     new ol.layer.VectorTile({
    //         style:styleRegion,
    //         source: new ol.source.VectorTile({
    //           tilePixelRatio: 1, // oversampling when > 1
    //           tileGrid: ol.tilegrid.createXYZ({maxZoom: 13}),
    //           format: new ol.format.MVT(),
    //           url: 'http://dev.geospatial.team/geoserver/gwc/service/tms/1.0.0/' + layerWaterbody +
    //           '@EPSG%3A'+projection_epsg_no+'@pbf/{z}/{x}/{-y}.pbf'
    //         })
    //     }),
    //     new ol.layer.VectorTile({
    //         style:styleRegion,
    //         source: new ol.source.VectorTile({
    //           tilePixelRatio: 1, // oversampling when > 1
    //           tileGrid: ol.tilegrid.createXYZ({maxZoom: 13}),
    //           format: new ol.format.MVT(),
    //           url: 'http://dev.geospatial.team/geoserver/gwc/service/tms/1.0.0/' + layerRegion +
    //           '@EPSG%3A'+projection_epsg_no+'@pbf/{z}/{x}/{-y}.pbf'
    //         })
    //     })
    //   ]
    // });



    var City = new ol.layer.VectorTile({
        source: new ol.source.VectorTile({
          tilePixelRatio: 1, // oversampling when > 1
          tileGrid: ol.tilegrid.createXYZ({maxZoom: 13}),
          format: new ol.format.MVT(),
          //projection: 'EPSG:3857',
          url: 'http://dev.geospatial.team/geoserver/gwc/service/tms/1.0.0/' + layerCity + '@EPSG%3A' + epsg_projection +'@pbf/{z}/{x}/{-y}.pbf'
        }),
        // style:styleCity,
        style: function(feature) { //sdsds
          return styleCity = new ol.style.Style({
                  image: new ol.style.RegularShape({
                      stroke: new ol.style.Stroke({
                        color: 'white',
                        width: 1.5
                      }),
                      fill: new ol.style.Fill({
                        color: 'black'
                      }),
                      radius: 5,
                      points: 4,
                      angle: Math.PI / 4,
                 }),
                 text: new ol.style.Text({
                    stroke: new ol.style.Stroke({
                       color: '#ffffff',
                       width: 2.5
                     }),
                    // fill: new ol.style.Fill({
                    //    color: 'f2cb00',
                    //    width: 2.5
                    //  }),
                    font: 'bold 14px Roboto, sans-serif',
                    text: feature.get("name").toString(),
                    offsetX: 17,
                    offsetY: -12
                })
            })
        }
      });
      var Region = new ol.layer.VectorTile({
          source: new ol.source.VectorTile({
            tilePixelRatio: 1, // oversampling when > 1
            tileGrid: ol.tilegrid.createXYZ({maxZoom: 13}),
            format: new ol.format.MVT(),
            // defaultDataProjection: ol.proj.get('EPSG:4326'),
            url: 'http://dev.geospatial.team/geoserver/gwc/service/tms/1.0.0/' + layerRegion + '@EPSG%3A' + epsg_projection +'@pbf/{z}/{x}/{-y}.pbf'
          }),
          // style:styleRegion,
          style: function(feature, resolution) {
            var polyStyleConfig = {
              stroke: new ol.style.Stroke({
                  color: 'rgba(223, 80, 153, 1)',
                  width: 3
              }),
            }
            var textStyleConfig = {//textRegion = {
              text: new ol.style.Text({
                text: resolution < 100000 ? feature.get('name').toString() : '' , //feature.get("name").toString(),
                stroke: new ol.style.Stroke({
                  color: '#f2006d',
                  width: 2.5
                  }),
                //font: 'bold 14px Roboto, sans-serif',
             })
          }
          var styleRegion = new ol.style.Style(polyStyleConfig);
          var textRegion = new ol.style.Style(textStyleConfig);
          return [styleRegion, textRegion];
        }
      });
    var Waterbody = new ol.layer.VectorTile({
          // style:styleWaterbody,
          source: new ol.source.VectorTile({
            tilePixelRatio: 1, // oversampling when > 1
            tileGrid: ol.tilegrid.createXYZ({maxZoom: 13}),
            format: new ol.format.MVT(),
            // defaultDataProjection: ol.proj.get('EPSG:4326'),
            url: 'http://dev.geospatial.team/geoserver/gwc/service/tms/1.0.0/' + layerWaterbody + '@EPSG%3A' + epsg_projection +'@pbf/{z}/{x}/{-y}.pbf'
          }),
          style: function(feature) { //sdsds
            return  styleWaterbody = new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(5, 144, 212, 0.5)',//#0590d4
                        opacity: 0.5
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(0, 107, 217, 0.7)',
                        width: 1,
                        opacity: 0.7
                    }),
                    text: new ol.style.Text({
                       // stroke: new ol.style.Stroke({
                       //    color: '#f2cb00',
                       //    width: 2.5
                       //  }),
                       // textAlign: 'center',
                       font: 'bold 14px Roboto, sans-serif',
                       text: feature.get("name").toString(),
                       // offsetX: 15,
                       // offsetY: -12
                     })
                   })
                 }
    });

    // var Terrain = new ol.layer.Tile({
    //     source: new ol.source.XYZ({
    //       url: 'http://dev.geospatial.team/geoserver/gwc/service/wms?',
    //       projection: 4326,
    //       tilePixelRatio: 1,
    //
    //     })
    // });

    var Terrain = new ol.layer.Tile({
        source: new ol.source.TileWMS({
          url: 'http://dev.geospatial.team/geoserver/gwc/service/wms?',
          params: {
            'LAYERS': layerTerrain,
            // 'CRS': 'EPSG:900913',
            // 'CRS': 'EPSG:3857',
            'TILED': true,
            'VERSION': '1.1.1',
            'FORMAT': 'image/png8'
          }
      })
    });


    var map = new ol.Map({
        target: 'map-frame',
        view: new ol.View({
            projection: 'EPSG:3857',
            // center: [0,0],
            // extent: ol.proj.transform([-90, -180, 90, 180],
            //         'EPSG:4326', 'EPSG:3857'),
          //  extent: ol.proj.get("EPSG:3857").getExtent(),
            center: [-26.103, -4.195], //Deois
            zoom: 1,
            minZoom: 1,
            maxZoom: 6
        }),
        layers: [Terrain, Waterbody, Region, City]
    });

  //   map.on('click', function(e) {
  //   let markup = '';
  //   map.forEachFeatureAtPixel(e.pixel, function(feature) {
  //     markup += `${markup && '<hr>'}<table>`;
  //     const properties = feature.getProperties();
  //     for (const property in properties) {
  //       markup += `<tr><th>${property}</th><td>${properties[property]}</td></tr>`;
  //     }
  //     markup += '</table>';
  //   }, {hitTolerance: 1});
  //   if (markup) {
  //     document.getElementById('popup-content').innerHTML = markup;
  //     overlay.setPosition(e.coordinate);
  //   } else {
  //     overlay.setPosition();
  //   }
  // });
  //
  //   const overlay = new Overlay({
  //     element: document.getElementById('popup-container'),
  //     positioning: 'bottom-center',
  //     offset: [0, -10],
  //     autoPan: true
  //   });
  //   map.addOverlay(overlay);
  //
  //   overlay.getElement().addEventListener('click', function() {
  //   overlay.setPosition();
  // });
  // -- Display information on singleclick --

// Create a popup overlay which will be used to display feature info
// var popup = new ol.Overlay.popup();
// map.addOverlay(popup);



// Add an event handler for the map "singleclick" event
map.on('singleclick', function(evt) {

    // Hide existing popup and reset it's offset
    popup.hide();
    popup.setOffset([0, 0]);

    // Attempt to find a feature in one of the visible vector layers
    var feature = map.forEachFeature(function(feature, layer) {
        return feature;
    });

    if (feature) {

        // var coord = feature.getGeometry().getCoordinates();
        var props = feature.getProperties();
        var info = "<h2><a href='" + props.name + "'>" + props.id + "</a></h2>";
        // Offset the popup so it points at the middle of the marker not the tip
        popup.setOffset([0, -22]);
        popup.show(coord, info);

    }

});

}

document.addEventListener('DOMContentLoaded', init);
