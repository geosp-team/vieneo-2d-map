# README #

![Logo of "Rise: The Vieneo Province" Game](http://rise.unistellar.com/images/1/16/Risetitle.jpg)

This repo contains a **2D** map of **Vieneo** - a moon in the virtual universe of the **Rise: The Vieneo Province** computer game. The web map application is written in HTML/CSS/JavaScript and based on the
[OpenLayers](https://openlayers.org/) library. Read more about the game:

* [Official website](http://rise.unistellar.com/)
* [Wikipedia](https://en.wikipedia.org/wiki/Rise:_The_Vieneo_Province)

### Repo Structure ###

* `pictures/*` - folder with pictures for decoration of the web page 
* `index.html` - self-describing
* `main.css` - styling of the web page
* `map.js` - script to build the map, containing definition and styles for all layers 
* `ol-controls.js` - custom styling of OpenLayers control tools
* `test_4326prj.html` - a file for testing another coordinate reference system

### Authors ###

* Repo owner, admin and map developer: [Yaroslav Vasyunin, Russia](https://www.linkedin.com/in/vasyunin/)
* Map developer: Oleg Bulantsev, Russia
* Game developers: [Unistellar Industries Ltd., USA](http://www.unistellar.com/)